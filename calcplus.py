# Noelia Arroyo Castaño, Práctica2_Ejercicio6
# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("Usage: calcplus.py fichero")

    archivo = open(sys.argv[1])
    lineas = archivo.readlines()

    for linea in lineas:
        datos = linea.split(",")
        print(datos[0])
        operador = datos[0]
        result = datos[1]

        for operandos in datos[2:]:
            ob3 = calcoohija.CalculadoraHija(operador, result, operandos)
            result = ob3.Operar()
        print(result)
