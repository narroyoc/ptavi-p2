# Noelia Arroyo Castaño, Práctica2_Ejercicio7
# !/usr/bin/python3
# -*- coding: utf-8 -*-


import sys
import calcoohija
import csv

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("Usage: calcplusplus.py fichero")

    with open(sys.argv[1]) as csvfile:

        archivo = csv.reader(csvfile)

        for datos in archivo:
            operador = datos[0]
            result = datos[1]

            for operando in datos[2:]:
                ob4 = calcoohija.CalculadoraHija(operador, result, operando)
                result = ob4.Operar()
            print(result)
