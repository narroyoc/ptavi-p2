# Noelia Arroyo Castaño, Práctica2_Ejercicio4
# !/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():

    def __init__(self, operador, operando1, operando2):

        self.operador = operador
        self.operando1 = operando1
        self.operando2 = operando2

    def suma(self):
        return float(self.operando1) + float(self.operando2)

    def resta(self):
        return float(self.operando1) - float(self.operando2)

    def Operar(self):

        if self.operador == 'suma':
            return self.suma()

        elif self.operador == 'resta':
            return self.resta()

        else:
            return "operación no válida"


if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.exit("Usage: calcoo.py operando1 operacion operando2")

    operador = sys.argv[2]
    operando1 = sys.argv[1]
    operando2 = sys.argv[3]

    try:

        operando1 = float(operando1)
        operando2 = float(operando2)

    except ValueError:

        sys.exit("Solo se aceptan números enteros y decimales")

    ob1 = Calculadora(operador, operando1, operando2)
    solucion = ob1.Operar()

    print(solucion)
